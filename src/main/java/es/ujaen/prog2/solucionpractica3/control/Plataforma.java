/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica3.control;

import es.ujaen.prog2.solucionpractica3.data.recursos.Asignatura;
import es.ujaen.prog2.solucionpractica3.data.recursos.Curso;
import es.ujaen.prog2.solucionpractica3.data.recursos.Foro;
import es.ujaen.prog2.solucionpractica3.data.recursos.Recurso;
import es.ujaen.prog2.solucionpractica3.data.usuarios.Alumno;
import es.ujaen.prog2.solucionpractica3.data.usuarios.Profesor;
import es.ujaen.prog2.solucionpractica3.data.usuarios.Usuario;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Plataforma {

    private String nombre;
    private String direccion;
    private String telefono;
    private String cif;
    private String email;
    private ArrayList<Usuario> usuarios;
    private ArrayList<Recurso> recursos;

    public Plataforma(String nombre, String direccion, String telefono, String cif, String email) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.cif = cif;
        this.email = email;
        usuarios = new ArrayList<>();
        recursos = new ArrayList<>();
    }

    public boolean altaRecurso(Recurso recurso) {
        for (Recurso rec : recursos) {
            if (rec.getIdentificador().equals(recurso.getIdentificador())) {
                return false;
            }
        }

        recursos.add(recurso);
        return true;
    }

    public boolean bajaRecurso(String identificador) {
        for (int i = 0; i < recursos.size(); i++) {
            if (recursos.get(i).getIdentificador().equals(identificador)) {
                recursos.remove(i);
                return true;
            }
        }

        return false;
    }

    public boolean altaUsuario(Usuario usuario) {
        for (Usuario usu : usuarios) {
            if (usu.getIdentificador().equals(usuario.getIdentificador())) {
                return false;
            }
        }

        usuarios.add(usuario);
        return true;
    }

    public boolean bajaUsuario(String identificador) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getIdentificador().equals(identificador)) {
                usuarios.remove(i);
                return true;
            }
        }

        return false;
    }

    public String getInfoRecursos(int tipoRecurso) {
        Class filtro;

        switch (tipoRecurso) {
            case 0:
                filtro = Recurso.class;
                break;
            case 1:
                filtro = Asignatura.class;
                break;
            case 2:
                filtro = Curso.class;
                break;
            case 3:
                filtro = Foro.class;
                break;
            default:
                filtro = Recurso.class;
        }

        String resultado = "";

        for (Recurso recurso : recursos) {
            if (recurso.getClass().equals(filtro)) {
                resultado += recurso.to_string() + "\n";
            }
        }

        return resultado;
    }

    public Recurso getRecurso(String idRecurso) {
        for (Recurso recurso : recursos) {
            if (recurso.getIdentificador().equals(idRecurso)) {
                return recurso;
            }
        }

        return null;
    }

    public String getInfoUsuario(int tipoUsuario) {
        Class filtro;

        switch (tipoUsuario) {
            case 0:
                filtro = Usuario.class;
                break;
            case 1:
                filtro = Profesor.class;
                break;
            case 2:
                filtro = Alumno.class;
                break;
            default:
                filtro = Usuario.class;
        }

        String resultado = "";

        for (Usuario usuario : usuarios) {
            if (usuario.getClass().equals(filtro)) {
                resultado += usuario.to_string() + "\n";
            }
        }

        return resultado;
    }

    public Usuario getUsuario(String idUsuario) {
        for (Usuario usuario : usuarios) {
            if (usuario.getIdentificador().equals(idUsuario)) {
                return usuario;
            }
        }
        return null;
    }

    public boolean matricularAlumno(String idRecurso, String idUsuario) {
        Recurso recurso = null;
        Alumno alumno = null;

        for (Recurso rec : recursos) {
            if (rec.getIdentificador().equals(idRecurso)) {
                recurso = rec;
            }
        }

        for (Usuario usu : usuarios) {
            if (usu.getIdentificador().equals(idUsuario) && usu.getClass().equals(Alumno.class)) {
                alumno = (Alumno) usu;
            }
        }

        if (recurso != null && alumno != null) {
            return recurso.matricularAlumno(alumno);
        } else {
            return false;
        }
    }

    public boolean asignarResponsable(String idRecurso, String idUsuario) {
        Recurso recurso = null;
        Profesor profesor = null;

        for (Recurso rec : recursos) {
            if (rec.getIdentificador().equals(idRecurso)) {
                recurso = rec;
            }
        }

        for (Usuario usu : usuarios) {
            if (usu.getIdentificador().equals(idUsuario) && usu.getClass().equals(Profesor.class)) {
                profesor = (Profesor) usu;
            }
        }

        if (recurso != null && profesor != null) {
            recurso.cambiarProfesorResponsable(profesor);
            return true;
        } else {
            return false;
        }
    }

    public boolean modificarUsuario(String idUsuario, String nombre, String telefono,
            String dni, String email, String gradoMat, boolean matrículaPagada,
            float notaMedia) {

        Alumno alumno = null;

        for (Usuario usu : usuarios) {
            if (usu.getIdentificador().equals(idUsuario) && usu.getClass().equals(Alumno.class)) {
                alumno = (Alumno) usu;
            }
        }

        if (alumno == null) {
            return false;
        }

        alumno.setNombre(nombre);
        alumno.setTelefono(telefono);
        alumno.setDni(dni);
        alumno.setEmail(email);
        alumno.setGradoMat(gradoMat);
        alumno.setMatriculaPagada(matrículaPagada);
        alumno.setNotaMedia(notaMedia);
        return true;
    }

    public boolean modificarUsuario(String idUsuario, String nombre, String telefono,
            String dni, String email, boolean contratoActivo, float créditosAsignados) {

        Profesor profesor = null;

        for (Usuario usu : usuarios) {
            if (usu.getIdentificador().equals(idUsuario) && usu.getClass().equals(Profesor.class)) {
                profesor = (Profesor) usu;
            }
        }

        if (profesor == null) {
            return false;
        }

        profesor.setNombre(nombre);
        profesor.setTelefono(telefono);
        profesor.setDni(dni);
        profesor.setEmail(email);
        profesor.setCreditosAsignados(créditosAsignados);
        profesor.setContratoActivo(contratoActivo);
        return true;
    }

    public boolean modificarRecurso(String idRecurso, String titulo,
            String facultad, String grado) {

        Asignatura asignatura = null;

        for (Recurso rec : recursos) {
            if (rec.getIdentificador().equals(idRecurso) && rec.getClass().equals(Asignatura.class)) {
                asignatura = (Asignatura) rec;
            }
        }

        if (asignatura == null) {
            return false;
        }

        asignatura.setTitulo(titulo);
        asignatura.setFacultadResponsable(facultad);
        asignatura.setGradoDondeSeImparte(grado);

        return true;
    }

    public boolean modificarRecurso(String idRecurso, String titulo,
            int caractPermitidos, boolean permitirRespuestas) {

        Foro foro = null;

        for (Recurso rec : recursos) {
            if (rec.getIdentificador().equals(idRecurso) && rec.getClass().equals(Foro.class)) {
                foro = (Foro) rec;
            }
        }

        if (foro == null) {
            return false;
        }

        foro.setTitulo(titulo);
        foro.setCaractPermitidos(caractPermitidos);
        foro.setPermitirRespuestas(permitirRespuestas);

        return true;
    }

    public boolean modificarRecurso(String idRecurso, String titulo, int limitePlazas,
            float duracionMeses, float creditosImpartidos) {

        Curso curso = null;

        for (Recurso rec : recursos) {
            if (rec.getIdentificador().equals(idRecurso) && rec.getClass().equals(Curso.class)) {
                curso = (Curso) rec;
            }
        }

        if (curso == null) {
            return false;
        }

        curso.setTitulo(titulo);
        curso.setCreditosImpartidos(creditosImpartidos);
        curso.setDuracionMeses(duracionMeses);
        curso.setLimitePlazas(limitePlazas);

        return true;
    }

    public String to_string() {

        String resultado = "Plataforma " + nombre + " | " + direccion
                + " | " + telefono + " | " + email + " | " + cif;

        if (usuarios.size() > 0) {
            resultado += "\n$Usuarios registrados:";

            for (Usuario usuario : usuarios) {
                resultado += "\n\t" + usuario.to_string();
            }

        } else {
            resultado += "\n$Sin usuarios registrados.";
        }

        if (recursos.size() > 0) {
            resultado += "\n$Recursos registrados:";

            for (Recurso recurso : recursos) {
                resultado += "\n\t" + recurso.to_string();
            }

        } else {
            resultado += "\n$Sin recursos registrados.";
        }

        return resultado;
    }

}
