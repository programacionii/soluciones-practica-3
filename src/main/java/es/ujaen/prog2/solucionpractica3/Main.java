/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica3;

import es.ujaen.prog2.solucionpractica3.control.Plataforma;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Main {
    
    public static void main(String[] args) {
        
        Plataforma platea = new Plataforma("Universidad de Jaén", "Campus Las Lagunillas, s/n, 23071", "953210000", "Q12345678R", "info@ujaen.es");
        
        System.out.println(platea.to_string());
        
    }
    
}
