/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica3.data.recursos;

import es.ujaen.prog2.solucionpractica3.data.usuarios.Alumno;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Asignatura extends Recurso {

    private String facultadResponsable;
    private String gradoDondeSeImparte;

    public Asignatura(String facultadResponsable, String gradoDondeSeImparte, String titulo, String identificador) {
        super(titulo, identificador);
        this.facultadResponsable = facultadResponsable;
        this.gradoDondeSeImparte = gradoDondeSeImparte;
    }

    public String getFacultadResponsable() {
        return facultadResponsable;
    }

    public void setFacultadResponsable(String facultadResponsable) {
        this.facultadResponsable = facultadResponsable;
    }

    public String getGradoDondeSeImparte() {
        return gradoDondeSeImparte;
    }

    public void setGradoDondeSeImparte(String gradoDondeSeImparte) {
        this.gradoDondeSeImparte = gradoDondeSeImparte;
    }

    @Override
    public String to_string() {
        String resultado = "Asignatura " + getIdentificador() + " | " + getTitulo()
                + " #Impartido en " + gradoDondeSeImparte + " #En la facultad " + facultadResponsable
                + " $Profesor responsable: " + getProfesorResponsable();

        if (getAlumnosMatriculados().size() > 0) {
            resultado += " $Alumnos matriculados: ";

            for (Alumno alumno : getAlumnosMatriculados()) {

                resultado += " -" + alumno.getNombre();
            }

        } else {
            resultado += " $Sin alumnos matriculados.";
        }
        return resultado;
    }

}
