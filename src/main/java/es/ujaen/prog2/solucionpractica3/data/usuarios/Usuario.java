/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica3.data.usuarios;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public abstract class Usuario {

    private String identificador;
    private String nombre;
    private String telefono;
    private String dni;
    private String email;

    public Usuario(String identificador, String nombre, String telefono, String dni, String email) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.telefono = telefono;
        this.dni = dni;
        this.email = email;
    }

    public String getIdentificador() {
        return identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public abstract String to_string();

}
