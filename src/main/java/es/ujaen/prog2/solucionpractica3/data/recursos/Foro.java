/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica3.data.recursos;

import es.ujaen.prog2.solucionpractica3.data.usuarios.Alumno;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Foro extends Recurso {

    private int caractPermitidos;
    private boolean permitirRespuestas;

    public Foro(int caractPermitidos, boolean permitirRespuestas, String titulo, String identificador) {
        super(titulo, identificador);
        this.caractPermitidos = caractPermitidos;
        this.permitirRespuestas = permitirRespuestas;
    }

    public int getCaractPermitidos() {
        return caractPermitidos;
    }

    public void setCaractPermitidos(int caractPermitidos) {
        this.caractPermitidos = caractPermitidos;
    }

    public boolean isPermitirRespuestas() {
        return permitirRespuestas;
    }

    public void setPermitirRespuestas(boolean permitirRespuestas) {
        this.permitirRespuestas = permitirRespuestas;
    }

    @Override
    public String to_string() {
        String resultado = "Foro " + getIdentificador() + " | " + getTitulo()
                + " #" + caractPermitidos + " Carácteres permitidos"
                + " #" + (permitirRespuestas ? "Si" : "No") + " se permiten respuestas"
                + " $Profesor responsable: " + getProfesorResponsable();

        if (getAlumnosMatriculados().size() > 0) {
            resultado += " $Alumnos matriculados: ";

            for (Alumno alumno : getAlumnosMatriculados()) {

                resultado += " -" + alumno.getNombre();
            }

        } else {
            resultado += " $Sin alumnos matriculados.";
        }
        return resultado;
    }

}
