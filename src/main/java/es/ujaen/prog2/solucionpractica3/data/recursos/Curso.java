/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica3.data.recursos;

import es.ujaen.prog2.solucionpractica3.data.usuarios.Alumno;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Curso extends Recurso {

    private int limitePlazas;
    private float duracionMeses;
    private float creditosImpartidos;

    public Curso(int limitePlazas, float duracionMeses, float creditosImpartidos, String titulo, String identificador) {
        super(titulo, identificador);
        this.limitePlazas = limitePlazas;
        this.duracionMeses = duracionMeses;
        this.creditosImpartidos = creditosImpartidos;
    }

    public int getLimitePlazas() {
        return limitePlazas;
    }

    public void setLimitePlazas(int limitePlazas) {
        this.limitePlazas = limitePlazas;
    }

    public float getDuracionMeses() {
        return duracionMeses;
    }

    public void setDuracionMeses(float duracionMeses) {
        this.duracionMeses = duracionMeses;
    }

    public float getCreditosImpartidos() {
        return creditosImpartidos;
    }

    public void setCreditosImpartidos(float creditosImpartidos) {
        this.creditosImpartidos = creditosImpartidos;
    }

    @Override
    public String to_string() {
        String resultado = "Curso " + getIdentificador() + " | " + getTitulo()
                + " #" + limitePlazas + " plazas límite"
                + " #" + duracionMeses + " meses de docencia"
                + " #" + creditosImpartidos + " créditos asignados"
                + " $Profesor responsable: " + getProfesorResponsable();

        if (getAlumnosMatriculados().size() > 0) {
            resultado += " $Alumnos matriculados: ";

            for (Alumno alumno : getAlumnosMatriculados()) {

                resultado += " -" + alumno.getNombre();
            }

        } else {
            resultado += " $Sin alumnos matriculados.";
        }
        return resultado;
    }

}
