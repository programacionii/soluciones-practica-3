/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica3.data.recursos;

import es.ujaen.prog2.solucionpractica3.data.usuarios.Alumno;
import es.ujaen.prog2.solucionpractica3.data.usuarios.Profesor;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public abstract class Recurso {

    private String titulo;
    private String identificador;
    private ArrayList<Alumno> alumnosMatriculados;
    private Profesor profesorResponsable;

    public Recurso(String titulo, String identificador) {
        this.titulo = titulo;
        this.identificador = identificador;
        alumnosMatriculados = new ArrayList<>();
        profesorResponsable = null;
    }

    public boolean matricularAlumno(Alumno alumno) {
        for (Alumno alumnoMatriculado : alumnosMatriculados) {
            if(alumnoMatriculado.getIdentificador().equals(alumno.getIdentificador()))
                return false;
        }
        alumnosMatriculados.add(alumno);
        alumno.anadirMatriculaRecurso(this);
        return true;
    }

    public void cambiarProfesorResponsable(Profesor profesor) {
        if (profesorResponsable != null) {
            profesorResponsable.quitarRespRecurso(this);
        }
        profesorResponsable = profesor;
        profesorResponsable.anadirRespRecurso(this);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getIdentificador() {
        return identificador;
    }

    public ArrayList<Alumno> getAlumnosMatriculados() {
        return alumnosMatriculados;
    }

    protected void setAlumnosMatriculados(ArrayList<Alumno> alumnosMatriculados) {
        this.alumnosMatriculados = alumnosMatriculados;
    }

    protected Profesor getProfesorResponsable() {
        return profesorResponsable;
    }

    public abstract String to_string();

}
