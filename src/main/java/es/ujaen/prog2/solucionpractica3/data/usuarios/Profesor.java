/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica3.data.usuarios;

import es.ujaen.prog2.solucionpractica3.data.recursos.Recurso;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Profesor extends Usuario {

    private boolean contratoActivo;
    private float creditosAsignados;
    private ArrayList<Recurso> recursosResponsable;

    public Profesor(boolean contratoActivo, float creditosAsignados, String identificador, String nombre, String telefono, String dni, String email) {
        super(identificador, nombre, telefono, dni, email);
        this.contratoActivo = contratoActivo;
        this.creditosAsignados = creditosAsignados;
        recursosResponsable = new ArrayList<>();
    }

    public void anadirRespRecurso(Recurso recurso) {
        recursosResponsable.add(recurso);
    }

    public void quitarRespRecurso(Recurso recurso) {
        for (int i = 0; i < recursosResponsable.size(); i++) {
            if (recursosResponsable.get(i).getIdentificador().equals(recurso.getIdentificador())) {
                recursosResponsable.remove(i);
                break; //Se terminar el bucle al encontrar el recurso
            }
        }

        recursosResponsable.add(recurso);
    }

    public boolean isContratoActivo() {
        return contratoActivo;
    }

    public void setContratoActivo(boolean contratoActivo) {
        this.contratoActivo = contratoActivo;
    }

    public float getCreditosAsignados() {
        return creditosAsignados;
    }

    public void setCreditosAsignados(float creditosAsignados) {
        this.creditosAsignados = creditosAsignados;
    }

    @Override
    public String to_string() {
        String resultado = "Profesor " + getIdentificador() + " | " + getNombre()
                + " | " + getDni() + " | " + getEmail() + " | " + getTelefono()
                + " #Con " + creditosAsignados + " créditos asignados #" + (contratoActivo ? "Contrato en activo" : "Contrato NO activo");

        if (recursosResponsable.size() > 0) {
            resultado += " $Asignaturas de las que es responsable: ";

            for (Recurso recursoResponsable : recursosResponsable) {

                resultado += " -" + recursoResponsable.getTitulo();
            }

        } else {
            resultado += " $Sin asignaturas asignadas.";
        }

        return resultado;
    }

}
