/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica3.data.usuarios;

import es.ujaen.prog2.solucionpractica3.data.recursos.Recurso;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Alumno extends Usuario {

    private String gradoMat;
    private boolean matriculaPagada;
    private float notaMedia;
    private ArrayList<Recurso> recursosMatriculados;

    public Alumno(String gradoMat, String identificador, String nombre, String telefono, String dni, String email) {
        super(identificador, nombre, telefono, dni, email);
        this.gradoMat = gradoMat;
        recursosMatriculados = new ArrayList<>();
        notaMedia = 0;
        matriculaPagada = false;
    }

    public void anadirMatriculaRecurso(Recurso matriculado) {
        recursosMatriculados.add(matriculado);
    }

    public boolean isMatriculaPagada() {
        return matriculaPagada;
    }

    public void setMatriculaPagada(boolean matriculaPagada) {
        this.matriculaPagada = matriculaPagada;
    }

    public float getNotaMedia() {
        return notaMedia;
    }

    public void setNotaMedia(float notaMedia) {
        this.notaMedia = notaMedia;
    }

    public String getGradoMat() {
        return gradoMat;
    }

    public void setGradoMat(String gradoMat) {
        this.gradoMat = gradoMat;
    }
    
    @Override
    public String to_string() {
        String resultado = "Alumno " + getIdentificador() + " | " + getNombre()
                + " | " + getDni() + " | " + getEmail() + " | " + getTelefono()
                + " #Alumno de " + gradoMat + " #Nota Media: " + notaMedia
                + " (" + (matriculaPagada ? "Matrícula pagada" : "MÁTRICULA NO PAGADA") + ")";

        if (recursosMatriculados.size() > 0) {
            resultado += " $Asignaturas matriculadas: ";

            for (Recurso recursoMatriculado : recursosMatriculados) {

                resultado += " -" + recursoMatriculado.getTitulo();
            }

        } else {
            resultado += " $SIN asignaturas matriculadas.";
        }

        return resultado;
    }
}
